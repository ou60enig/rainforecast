'''READING DATA'''

#Load the csv file as data frame.
import numpy as np # linear algebra
import pandas as pd # data processing
from scipy import stats
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn import preprocessing
from Classifications import FeatureSelection
import datetime as dt
import matplotlib.pyplot as plt
from Classifications import NeuralNetwork, RandomForest, Accuracys, ConfusionMatrix, PlotConfusionMatrix,OptimizeParameter,OptimizeTestTrainRatio
from sklearn.model_selection import validation_curve, train_test_split

df = pd.read_csv('./weatherAUS.csv',sep=";")
print('Original DataSize:',df.shape)
print(df.count().sort_values())

'''DATA PREPROCESSING'''

# Change categorical cloumns
df['RainToday'].replace({'No': 0, 'Yes': 1},inplace = True)
df['RainTomorrow'].replace({'No': 0, 'Yes': 1},inplace = True)
df['WindDir3pm'].replace({'N':0,'NNE':0.0625,'NE':0.125,'ENE':0.1875,'E':0.25,'ESE':0.3125,'SE':0.375,'SSE':0.4375,'S':0.5,'SSW':0.5625,'SW':0.625,'WSW':0.6875,'W':0.75,'WNW':0.8215,'NW':0.875,'NNW':0.9375},inplace = True)
df['WindDir9am'].replace({'N':0,'NNE':0.0625,'NE':0.125,'ENE':0.1875,'E':0.25,'ESE':0.3125,'SE':0.375,'SSE':0.4375,'S':0.5,'SSW':0.5625,'SW':0.625,'WSW':0.6875,'W':0.75,'WNW':0.8215,'NW':0.875,'NNW':0.9375},inplace = True)

# Feature Selection, Drop Columns that will not be integrated into the model.
# Target variable: RAINTORMORROW
df['deltaWinddir']=df['WindDir3pm']-df['WindDir9am']
df['deltaWindV']=df['WindSpeed3pm']-df['WindSpeed9am']
df['deltaPressure']=df['Pressure3pm']-df['Pressure9am']
df['deltaHumidity']=df['Humidity3pm']-df['Humidity9am']
#df['month']=df['Date'].dt.strftime('%m')

#df = df.drop(columns=['WindGustDir','WindGustSpeed','Location','RISK_MM','Date'],axis=1)
df = df.drop(columns=['WindGustDir','RISK_MM','Location','Date'],axis=1)
#'WindDir9am','WindDir3pm','WindSpeed9am','WindSpeed3pm'

print('Size of the new data frame:',df.shape)

# Deleting Null Values
df = df.dropna(how='any')
print('Size of the new data frame:',df.shape)

#its time to remove the outliers in our data - we are using Z-score to detect and remove the outliers.
z = np.abs(stats.zscore(df._get_numeric_data()))
print(z)
df= df[(z < 3).all(axis=1)]
print('Size of the new data frame:',df.shape)

# Standardize data - using MinMaxScaler
scaler = preprocessing.MinMaxScaler()
scaler.fit(df)
df = pd.DataFrame(scaler.transform(df), index=df.index, columns=df.columns)

print(df[0:5])
print('Size of the new data frame:',df.shape)

#now that we are done with the pre-processing part, let's see which are the important features for RainTomorrow!
#Using SelectKBest to get the top features!
X = df.loc[:,df.columns!='RainTomorrow']
y = df[['RainTomorrow']]


X=FeatureSelection(14,X,y)

#Manuelle Feature Selection
#df = df[['Sunshine', 'Humidity3pm', 'Cloud9am', 'Cloud3pm', 'RainToday','RainTomorrow']]
#X = df[['Sunshine', 'Humidity3pm', 'Cloud9am', 'Cloud3pm', 'RainToday']]

print(X[0:5])
print(y[0:5])

#Classifications
#X_train, X_test, y_train, y_test=train_test_split(X,y,test_size=0.22)
#y_pred_MLP = NeuralNetwork(X_train, X_test, y_train, y_test, 60)
#Accuracys('Neural Network',y_test,y_pred_MLP)

#OptimizeParameter(RandomForestClassifier(),'n_estimators',range(1,500,25),X,y,1)
OptimizeTestTrainRatio(RandomForestClassifier(),"LearningCurve RF",X,y,n_jobs=1,train_sizes=[0.1, 0.33, 0.55, 0.78, 1.])


#cm = ConfusionMatrix(y_pred_MLP,y_test)

#y_pred_RF = RandomForest(X_train, X_test, y_train, y_test)
#Accuracys('Random Forest',y_test,y_pred_RF)

#Print ConfusionMatrix
#np.set_printoptions(precision=2)
#PlotConfusionMatrix(cm,['NoRain','Rain'],True,'Normalized ConfusionMatrix')
#plt.show()



'''
###Optimierung FeatureAuswahl###
acc=[]
for i in range(22):
    X_new=FeatureSelection(i+1,X,y)
    # Classifications

    from sklearn.model_selection import train_test_split
    from Classifications import NeuralNetwork, RandomForest, Accuracys
    X_train, X_test, y_train, y_test = train_test_split(X_new, y, test_size=0.25)
    y_pred_RF = RandomForest(X_train, X_test, y_train, y_test)
    acc.append(Accuracys('Random Forest', y_test, y_pred_RF))

import matplotlib.pyplot as plt
plt.plot(acc)
plt.ylabel('Accuracy')
plt.xlabel('Number of Features')
plt.show()
'''
'''
###Optimierung SplitVerhältnis###

from sklearn.model_selection import train_test_split
from Classifications import NeuralNetwork, RandomForest, Accuracys
acc=[]
X_new = FeatureSelection(14, X, y)
for i in range(1,20):
    # Classifications
    X_train, X_test, y_train, y_test = train_test_split(X_new, y, test_size=i/20)
    y_pred_RF = RandomForest(X_train, X_test, y_train, y_test)
    acc.append(Accuracys('Random Forest', y_test, y_pred_RF))

import matplotlib.pyplot as plt
plt.plot(acc)
plt.ylabel('Accuracy')
plt.xlabel('SplitVerhältnis x 20')
plt.show()
'''
'''
###Optimierung ParameterRandomForest###

from sklearn.model_selection import train_test_split
from Classifications import NeuralNetwork, RandomForest, Accuracys
acc=[]
X_new = FeatureSelection(14, X, y)
X_train, X_test, y_train, y_test = train_test_split(X_new, y, test_size=0.225)

for i in range(1,10):
    y_pred_RF = RandomForest(X_train, X_test, y_train, y_test,i)
    acc.append(Accuracys('Random Forest', y_test, y_pred_RF))

import matplotlib.pyplot as plt
plt.plot(acc)
plt.ylabel('Accuracy')
plt.xlabel('MaxDepth')
plt.show()
'''
'''
###Optimierung ParameterMLP###

from sklearn.model_selection import train_test_split
from Classifications import NeuralNetwork, RandomForest, Accuracys
acc=[]
X_new = FeatureSelection(14, X, y)
X_train, X_test, y_train, y_test = train_test_split(X_new, y, test_size=0.225)

for i in range(10,100,10):
    y_pred_MLP = NeuralNetwork(X_train, X_test, y_train, y_test,i)
    acc.append(Accuracys('MLP', y_test, y_pred_MLP))

#print(acc)
import matplotlib.pyplot as plt
plt.plot(acc)
plt.ylabel('Accuracy')
plt.xlabel('HiddenLayerSize X*X')
plt.show()
'''

