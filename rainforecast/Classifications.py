import numpy as np

def FeatureSelection(k, X,y):
    from sklearn.feature_selection import SelectKBest, chi2, f_classif
    selector = SelectKBest(f_classif, k=k)
    selector.fit(X, y)  # Run score function on (X, y) and get the appropriate features.
    X_new = selector.transform(X)  # Reduce X to the selected features. (numpy.ndarray)
    print(X.columns[selector.get_support(indices=True)])  # get_support Get a mask, or integer index, of the features selected

    return X_new

def RandomForest(X_train,X_test,y_train,y_test):
    from sklearn.ensemble import RandomForestClassifier
    import time
    import numpy as np
    from sklearn.feature_selection import SelectFromModel
    # Random Forest
    t0 = time.time()
    clf_rf = RandomForestClassifier(n_estimators=100, max_depth=6, random_state=0)
    clf_rf.fit(X_train, y_train)  # Training
    y_pred = clf_rf.predict(X_test)

    print('Time taken for RandomForest Classification:', time.time() - t0)

    return y_pred

def NeuralNetwork(X_train, X_test, y_train, y_test,i):
    from sklearn.neural_network import MLPClassifier
    import time

    # MLPClassifier
    t0 = time.time()
    clf_MLP = MLPClassifier(solver='lbfgs', alpha=1e-4,hidden_layer_sizes=(i, i), random_state=1)
    # hidden_layer_sizes=(200,200,100), alpha=1e-4, solver='sgd', verbose=10, tol=1e-4, random_state=1,learning_rate_init=.1)

    clf_MLP.fit(X_train, y_train)  # Training
    y_pred = clf_MLP.predict(X_test)

    print('Time taken for MLP Classification:', time.time() - t0)

    return y_pred

def Accuracys (Classification,y_test, y_pred):
    from sklearn.metrics import accuracy_score
    score = accuracy_score(y_test, y_pred)  # Testen
    print('Accuracy of '+Classification+': ', score)
    #return score

def ConfusionMatrix (y_pred, y_test):
    from sklearn.metrics import confusion_matrix
    return confusion_matrix(y_pred, y_test)

def PlotConfusionMatrix(cm, classes, normalize=False, title=None):
    import numpy as np
    import matplotlib.pyplot as plt
    from sklearn import svm, datasets
    from sklearn.model_selection import train_test_split
    from sklearn.metrics import confusion_matrix
    from sklearn.utils.multiclass import unique_labels
    cmap = plt.cm.Blues

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    return ax

def OptimizeParameter (estimator, param_name, param_range, X,y,n_jobs):
    from sklearn.model_selection import validation_curve
    import matplotlib.pyplot as plt
    import numpy as np

    train_scores, test_scores = validation_curve(estimator,X,y,param_name,param_range,n_jobs=n_jobs)
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)
    plt.title("Validation Curve with RF")
    plt.xlabel(param_name)
    plt.ylabel("Score")
    plt.ylim(0.6, 1)
    lw = 2
    plt.semilogx(param_range, train_scores_mean, label="Training score",
                 color="darkorange", lw=lw)
    plt.fill_between(param_range, train_scores_mean - train_scores_std,
                     train_scores_mean + train_scores_std, alpha=0.2,
                     color="darkorange", lw=lw)
    plt.semilogx(param_range, test_scores_mean, label="Cross-validation score",
                 color="navy", lw=lw)
    plt.fill_between(param_range, test_scores_mean - test_scores_std,
                     test_scores_mean + test_scores_std, alpha=0.2,
                     color="navy", lw=lw)
    plt.legend(loc="best")
    plt.show()



def OptimizeTestTrainRatio(estimator, title, X, y, ylim=None, cv=None,
                        n_jobs=None, train_sizes=np.linspace(.1, 1.0, 5)):

    import matplotlib.pyplot as plt
    from sklearn.model_selection import learning_curve

    """
    Generate a simple plot of the test and training learning curve.

    Parameters
    ----------
    estimator : object type that implements the "fit" and "predict" methods
        An object of that type which is cloned for each validation.

    title : string
        Title for the chart.

    X : array-like, shape (n_samples, n_features)
        Training vector, where n_samples is the number of samples and
        n_features is the number of features.

    y : array-like, shape (n_samples) or (n_samples, n_features), optional
        Target relative to X for classification or regression;
        None for unsupervised learning.

    ylim : tuple, shape (ymin, ymax), optional
        Defines minimum and maximum yvalues plotted.

    cv : int, cross-validation generator or an iterable, optional
        Determines the cross-validation splitting strategy.
        Possible inputs for cv are:
          - None, to use the default 3-fold cross-validation,
          - integer, to specify the number of folds.
          - :term:`CV splitter`,
          - An iterable yielding (train, test) splits as arrays of indices.

        For integer/None inputs, if ``y`` is binary or multiclass,
        :class:`StratifiedKFold` used. If the estimator is not a classifier
        or if ``y`` is neither binary nor multiclass, :class:`KFold` is used.

        Refer :ref:`User Guide <cross_validation>` for the various
        cross-validators that can be used here.

    n_jobs : int or None, optional (default=None)
        Number of jobs to run in parallel.
        ``None`` means 1 unless in a :obj:`joblib.parallel_backend` context.
        ``-1`` means using all processors. See :term:`Glossary <n_jobs>`
        for more details.

    train_sizes : array-like, shape (n_ticks,), dtype float or int
        Relative or absolute numbers of training examples that will be used to
        generate the learning curve. If the dtype is float, it is regarded as a
        fraction of the maximum size of the training set (that is determined
        by the selected validation method), i.e. it has to be within (0, 1].
        Otherwise it is interpreted as absolute sizes of the training sets.
        Note that for classification the number of samples usually have to
        be big enough to contain at least one sample from each class.
        (default: np.linspace(0.1, 1.0, 5))
    """
    plt.figure()
    plt.title(title)
    if ylim is not None:
        plt.ylim(*ylim)
    plt.xlabel("Training examples")
    plt.ylabel("Score")
    train_sizes, train_scores, test_scores = learning_curve(
        estimator, X, y, cv=cv, n_jobs=n_jobs, train_sizes=train_sizes)
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)
    plt.grid()

    plt.fill_between(train_sizes, train_scores_mean - train_scores_std,
                     train_scores_mean + train_scores_std, alpha=0.1,
                     color="r")
    plt.fill_between(train_sizes, test_scores_mean - test_scores_std,
                     test_scores_mean + test_scores_std, alpha=0.1, color="g")
    plt.plot(train_sizes, train_scores_mean, 'o-', color="r",
             label="Training score")
    plt.plot(train_sizes, test_scores_mean, 'o-', color="g",
             label="Cross-validation score")

    plt.legend(loc="best")
    plt.show()